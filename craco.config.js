module.exports = {
    webpack: {
        configure: (webpackConfig) => {
            const cssModuleRule = {
                test: /\.module\.s[ac]ss$/i,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            modules: {
                                localIdentName: '[name]__[local]___[hash:base64:5]', // 고유한 클래스 이름 설정
                            },
                            importLoaders: 1,
                        },
                    },
                    'sass-loader',
                ],
            };

            const cssRule = {
                test: /\.s[ac]ss$/i,
                exclude: /\.module\.s[ac]ss$/i,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader',
                ],
            };

            // CSS 모듈 및 일반 CSS 로더 설정 추가
            const rules = webpackConfig.module.rules.find(rule => Array.isArray(rule.oneOf)).oneOf;

            rules.unshift(cssModuleRule);
            rules.unshift(cssRule);

            return webpackConfig;
        },
    },
};