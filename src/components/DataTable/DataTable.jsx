import React, { useMemo, useState, useCallback } from "react";
import { AgGridReact } from "ag-grid-react";
import "ag-grid-community/styles/ag-grid.css";
// import 'ag-grid-community/styles/ag-theme-balham.css';
// import "ag-grid-community/styles/ag-theme-alpine.css";
import 'ag-grid-community/styles/ag-theme-quartz.css';
// import 'ag-grid-community/styles/ag-theme-material.css';
import "./DataTable.scss";

//단순 렌더
const ActionCellRenderer = (props) => {
    return (
        <div className="btn_box">
            <button className="update_btn">수정</button>
            <button className="delete_btn">삭제</button>
        </div>
    );
};

//컬럼 필터 검색 조건에 체크박스는 엔터프라이즈만 사용가능 (유료)
const App = () => {
    const containerStyle = useMemo(() => ({ width: "100%", height: "800px" }),[]);
    const gridStyle = useMemo(() => ({ height: "100%", width: "100%" }), []);
    const [rowData, setRowData] = useState();
    const [columnDefs] = useState([
        { field: "athlete", minWidth: 150, },
        { field: "age", maxWidth: 90, },
        { field: "country", minWidth: 150, },
        { field: "year", maxWidth: 90 },
        { field: "date", minWidth: 150, },
        { field: "sport", minWidth: 150, },
        { field: "gold" },
        { field: "silver" },
        { field: "bronze" },
        { field: "total" },
        //
        {
            headerName: "Action",
            field: "action",
            cellRenderer: ActionCellRenderer,
            minWidth: 150,
            filter: null,
            floatingFilter: false,
        }
    ]);

    const defaultColDef = useMemo(() => {
        return {
            flex: 1,
            minWidth: 100,
            editable: true,
            filter: "agTextColumnFilter",
            floatingFilter: true,
        };
    }, []);

    const localeText = {
        contains: '포함',
        notContains: '포함하지 않음',
        equals: '같음',
        notEqual: '같지 않음',
        startsWith: '시작 문자',
        endsWith: '끝 문자',
        blank: '비어 있음',
        notBlank: '비어 있지 않음',
        page: '페이지',
        more: '더 보기',
        to: '부터',
        of: '까지',
        next: '다음',
        last: '마지막',
        first: '처음',
        previous: '이전',
        loadingOoo: '로딩 중...',
        pageSize: '페이지 크기',
        pageSizeChoose: '페이지 크기 선택',
        pageSizeAll: '모두',
        pagination: '페이지네이션',
        paginationPageSize: '페이지 크기',
        paginationPageSizeChoose: '페이지 크기 선택',
        noRowsToShow: '조회 결과가 없습니다.',
    };

    // 수정/삭제 버튼이 없다면 밑에 주석처리 된 함수
    const handleRowClicked = useCallback((event) => {
        if (event.event.target.classList.contains('update_btn')) {
            console.log('Edit clicked for', event.data);
        } else if (event.event.target.classList.contains('delete_btn')) {
            console.log('Delete clicked for', event.data);
        } else {
            console.log('클릭된 행 데이터:', event.data);
        }
    }, []);

    // const handleRowClicked = (event) => {
    //     console.log('클릭된 행 데이터:', event.data);
    // };

    const onGridReady = useCallback((params) => {
        const url = new URL("https://www.ag-grid.com/example-assets/olympic-winners.json");

        // 파라미터 추가
        url.searchParams.append('param1', 'value1');
        url.searchParams.append('param2', 'value2');
        // 필요에 따라 더 많은 파라미터를 추가할 수 있습니다.

        fetch(url)
            .then((resp) => resp.json())
            .then((data) => setRowData(data));
    }, []);

    return (
        <div style={containerStyle} className="p_dataTable_wrap">
            <div style={gridStyle} className="ag-theme-quartz">
                <AgGridReact
                    rowData={rowData} // 그리드에 표시할 데이터
                    columnDefs={columnDefs} // 그리드의 컬럼 정의
                    defaultColDef={defaultColDef} // 모든 컬럼에 공통으로 적용할 기본 설정
                    localeText={localeText} // 컬럼 필터 번역
                    rowSelection={'single'} // 행 선택 모드를 설정합니다. 'single' 또는 'multiple' 값
                    // enableRangeSelection={true}
                    enableClipboard={true} // 클립보드 기능을 활성화
                    rowGroupPanelShow={"always"} // 그룹 패널을 언제 표시할지 설정합니다. 'always', 'onlyWhenGrouping', 'never' 중 하나의 값
                    groupDisplayType={"groupRows"} // 그룹 표시 유형을 설정
                    animateRows={true} // 행 애니메이션
                    suppressAggFuncInHeader={true} // 헤더에 집계 함수 표시를 억제
                    pagination // 페이지네이션을 활성화
                    paginationPageSize={100} // 페이지당 행 수
                    onGridReady={onGridReady} // 그리드가 초기화된 후 호출되는 콜백 함수
                    onRowClicked={handleRowClicked}
                ></AgGridReact>
            </div>
        </div>
    );
};

export default App;
