import React, { useState, useRef } from 'react';
import './Editor.scss';
import { Editor } from '@tinymce/tinymce-react';
import axios from 'axios';
import common from '../../common';

const MyEditor = () => {
	const { useAlert } = common();
	const { pAlert, AlertComponent } = useAlert();
	const [title, setTitle] = useState('');
	const [files, setFiles] = useState([]);
	const editorRef = useRef(null);

	const handleFileChange = (e) => {
		setFiles([...files, ...Array.from(e.target.files)]);
	};

	const handleFileRemove = (index) => {
		setFiles(files.filter((_, i) => i !== index));
	};

	const handleSave = async () => {
		if (editorRef.current) {
			const content = editorRef.current.getContent();

			if (!title.trim()) {
				pAlert('제목을 입력해주세요.');
				return;
			}

			if (!content.trim()) {
				pAlert('내용을 입력해주세요.');
				return;
			}
			
			// FormData를 사용하여 데이터를 전송합니다.
			const formData = new FormData();
			formData.append('title', title);
			formData.append('contents', content);
			files.forEach((file, index) => {
				formData.append(`file${index}`, file);
			});

			for (let [key, value] of formData.entries()) {
				console.log(`${key}: ${value}`);
			}

			try {
				const response = await axios.post('https://yehs-api.accurbill.com/file/in/document_insert', formData, {
					headers: {
						'Content-Type': 'multipart/form-data',
					},
					withCredentials: true,
				});
				console.log('Response:', response.data);
			} catch (error) {
				console.error('Error:', error);
			}
		}
	};

	const imagesUploadHandler = async (blobInfo, success, failure) => {
		const formData = new FormData();
		formData.append('file', blobInfo.blob(), blobInfo.filename());

		for (let [key, value] of formData.entries()) {
			console.log(`${key}: ${value}`);
		}

		try {
			const response = await axios.post('https://yehs-api.accurbill.com/file/in/upload_image', formData, {
				headers: {
					'Content-Type': 'multipart/form-data',
				},
				withCredentials: true,
			});
			console.log('Image Upload Response:', response.data);
			if (response.data && response.data.url) {
				success(response.data.url); // 이미지 URL을 에디터에 삽입
				console.log('Image successfully inserted:', response.data.url);
			} else {
				failure('Invalid JSON response');
			}
		} catch (error) {
			failure('Image upload failed: ' + error.message);
		}
	};

	return (
		<>
			<AlertComponent />
			<div className="input_box">
				<label>제목</label>
				<input
					type="text"
					value={title}
					onChange={(e) => setTitle(e.target.value)}
				/>
			</div>
			<div className="input_box">
				<label>파일 첨부</label>
				<input
					type="file"
					multiple
					onChange={handleFileChange}
				/>
				<ul className="file_list">
					{files.map((file, index) => (
						<li key={index} className="file_item">
							{file.name}
							<button type="button" onClick={() => handleFileRemove(index)} className="remove_btn">삭제</button>
						</li>
					))}
				</ul>
			</div>
			<Editor
				apiKey='mra1l3khnm60zsfy1avqcyifvlaglm9lpp8fbe744405rpp2'
				onInit={(_evt, editor) => editorRef.current = editor}
				initialValue="<p>This is the initial content of the editor.</p>"
				init={{
					height: 500,
					menubar: false,
					plugins: [
						'advlist', 'autolink', 'lists', 'link', 'image', 'charmap', 'preview',
						'anchor', 'searchreplace', 'visualblocks', 'code', 'fullscreen',
						'insertdatetime', 'media', 'table', 'code', 'help', 'wordcount'
					],
					toolbar: 'fontfamily fontsize | bold underline italic strikethrough forecolor backcolor removeformat superscript subscript | bullist numlist alignleft aligncenter alignright alignjustify | lineheight | table | link image media | code preview help myCustomButton',
					images_upload_handler: imagesUploadHandler,
					content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
				}}
			/>
			<button onClick={handleSave}>Save</button>
		</>
	);
};

export default MyEditor;
