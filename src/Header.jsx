// src/components/Header.js
import React, { useState, useEffect } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import axios from 'axios';
import Accordion from 'react-bootstrap/Accordion';
const API_BASE_URL = process.env.REACT_APP_API_BASE_URL;

const formatDate = (date) => {
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0'); // 월은 0부터 시작하므로 +1
    const day = String(date.getDate()).padStart(2, '0');
    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');
    const seconds = String(date.getSeconds()).padStart(2, '0');

    return `${year}. ${month}. ${day} ${hours}:${minutes}:${seconds}`;
};

function Header() {
    const navigate = useNavigate();
    const location = useLocation();
    const [headerClass, setHeaderClass] = useState('');

    const [currentTime, setCurrentTime] = useState(formatDate(new Date()));

    useEffect(() => {
        const timer = setInterval(() => {
            setCurrentTime(formatDate(new Date()));
        }, 1000);

        return () => clearInterval(timer);
    }, []);

    useEffect(() => {
        // 경로에 따라 headerClass 상태를 업데이트
        switch (location.pathname) {
            case '/':
                setHeaderClass('main-header'); // 메인 페이지에 적용할 클래스
                break;
            case '/user':
                setHeaderClass('a-header'); // A 컴포넌트 페이지에 적용할 클래스
                break;
            default:
                setHeaderClass('');
        }
    }, [location.pathname]); // 위치가 변경될 때마다 실행

    const handleLogout = async (e) => {
        e.preventDefault();
        try {
            const response = await axios.post(`${API_BASE_URL}/admin_login/out/logout`, {}, {
                withCredentials: true
            });
            if (response.data.result === 't') {
                navigate('/login'); // 로그아웃 성공 시 로그인 페이지로 리디렉트
            } else {
                alert('로그아웃에 실패했습니다. 다시 시도해주세요.');
            }
        } catch (error) {
            console.error('Logout failed:', error);
            alert('로그아웃에 실패했습니다. 다시 시도해주세요.');
        }
    };

    return (
        <div className={`header_wrap ${headerClass}`}>
            <div className="flex_between header">
                <div className="hd_left_box display_flex">
                    <div className="hd_logo_img_box display_flex">
                        <img src="/img/logo.png" alt="" />
                    </div>
                    <span className='hd_user_name_txt f_normal f_500'>[소프트제이ㅇㅇㅇ]</span>
                    <span className='hd_user_txt f_normal f_white f_300' >&nbsp;님 환영합니다.</span>
                </div>
                <div className="hd_right_box display_flex">
                    <div className="hd_time_box">
                        <span className='f_big f_white'>{currentTime}</span>
                    </div>
                    <a href="https://www.naek.or.kr/" target="_blank" rel="noopener noreferrer" className="go_web_btn hd_btn">
                        <span className="f_small f_500">웹페이지</span>
                    </a>
                    <a href="/" onClick={handleLogout} className="logout_btn hd_btn">
                        <span className="f_small">로그아웃</span>
                    </a>
                </div>
            </div>
            <nav>
                <Accordion defaultActiveKey="0">
                    <Accordion.Item eventKey="0">
                        <Accordion.Header>
                            <Link className='nav_1_btn' to={'/main'}><span className='f_big f_white f_400'>main</span></Link>
                        </Accordion.Header>
                    </Accordion.Item>
                    <Accordion.Item eventKey="1">
                        <Accordion.Header>
                            <Link className='nav_1_btn' to={'/user'}><span className='f_big f_white f_400'>회원 관리</span></Link>
                        </Accordion.Header>
                        <Accordion.Body>
                            <Link className='nav_2_btn' to={'/user'}><span className='f_big f_white f_400'>회원정보관리</span></Link>
                            <Link className='nav_2_btn' to={'/user'}><span className='f_big f_white f_400'>회원학인증 발급내역</span></Link>
                        </Accordion.Body>
                    </Accordion.Item>
                    <Accordion.Item eventKey="2">
                        <Accordion.Header>
                            <Link className='nav_1_btn' to={'/eventCalendar'}><span className='f_big f_white f_400'>행사 관리</span></Link>
                        </Accordion.Header>
                        <Accordion.Body>
                            <Link className='nav_2_btn' to={'/eventCalendar'}><span className='f_big f_white f_400'>행사 캘린더</span></Link>
                            <Link className='nav_2_btn' to={'/eventList'}><span className='f_big f_white f_400'>행사 목록</span></Link>
                        </Accordion.Body>
                    </Accordion.Item>
                    <Accordion.Item eventKey="3">
                        <Accordion.Header>
                            <Link className='nav_1_btn' to={'/board'}><span className='f_big f_white f_400'>에디터</span></Link>
                        </Accordion.Header>
                    </Accordion.Item>
                </Accordion>
            </nav>
        </div>
    );
}

export default Header;
