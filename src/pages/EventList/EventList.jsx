import React, { useState, useEffect } from 'react';
import '../EventList/EventList.scss';
import { Editor } from "../../components";
import common from '../../common';

const { useDatePicker } = common();

function EventList() {
    const [homepageVisible, setHomepageVisible] = useState(false);
    const [participationButtonVisible, setParticipationButtonVisible] = useState(false);
    const [category, setCategory] = useState('NAEK포럼');
    const [title, setTitle] = useState('');
    const [location, setLocation] = useState('');
    const [fee, setFee] = useState('');
    const [date, setDate] = useState('');
    const [hour, setHour] = useState('00');
    const [minute, setMinute] = useState('00');
    const [editorData, setEditorData] = useState('');


    useEffect(() => {
        const datePickerElement = document.querySelector('.pDatePicker');
        if (datePickerElement) {
            setDate(datePickerElement.value);
        }
    }, []);

    const handleSave = () => {
        //현재 데이터피커 날짜
        const datePickerElement = document.querySelector('.pDatePicker');
        const selectedDate = datePickerElement ? datePickerElement.value : '';
        console.log({
            homepageVisible,
            participationButtonVisible,
            category,
            title,
            location,
            fee,
            date: selectedDate,
            time: `${hour}:${minute}`,
            editorData
        });
    };

    // 데이트피커
    const { DatePickerComponent } = useDatePicker();

    return (
        <>
            <div className="components_wrap">
                <div className="eventList_wrap">
                    <div className="form_group">
                        <label>홈페이지 노출</label>
                        <label className="switch">
                            <input type="checkbox" checked={homepageVisible} onChange={() => setHomepageVisible(!homepageVisible)} />
                            <span className="slider round"></span>
                        </label>
                    </div>
                    <div className="form_group">
                        <label>참가버튼 노출</label>
                        <label className="switch">
                            <input type="checkbox" checked={participationButtonVisible} onChange={() => setParticipationButtonVisible(!participationButtonVisible)} />
                            <span className="slider round"></span>
                        </label>
                    </div>
                    <div className="form_group">
                        <label>분류</label>
                        <select value={category} onChange={(e) => setCategory(e.target.value)}>
                            <option value="NAEK포럼">NAEK포럼</option>
                            <option value="다른 옵션">다른 옵션</option>
                        </select>
                    </div>
                    <div className="form_group">
                        <label>행사명</label>
                        <input type="text" placeholder="제목" value={title} onChange={(e) => setTitle(e.target.value)} />
                    </div>
                    <div className="form_group">
                        <label>장소</label>
                        <input type="text" placeholder="장소" value={location} onChange={(e) => setLocation(e.target.value)} />
                    </div>
                    <div className="form_group">
                        <label>참가비</label>
                        <input type="text" placeholder="원" value={fee} onChange={(e) => setFee(e.target.value)} />
                    </div>
                    <div className="form_group">
                        <label>일정</label>
                        <div className="date_time_group">
                            <DatePickerComponent
                                className="pDatePicker tmp_date"
                            />
                            <div className="time_select">
                                <select value={hour} onChange={(e) => setHour(e.target.value)}>
                                    {[...Array(24).keys()].map(n => (
                                        <option key={n} value={String(n).padStart(2, '0')}>{String(n).padStart(2, '0')}</option>
                                    ))}
                                </select>
                                <span> : </span>
                                <select value={minute} onChange={(e) => setMinute(e.target.value)}>
                                    {[...Array(60).keys()].map(n => (
                                        <option key={n} value={String(n).padStart(2, '0')}>{String(n).padStart(2, '0')}</option>
                                    ))}
                                </select>
                            </div>
                        </div>
                    </div>
                    <Editor initialData={editorData} onDataChange={setEditorData} />
                    <div className="form_group">
                        <button className="event_insert_btn" onClick={handleSave}>저장</button>
                    </div>
                </div>
            </div>
        </>
    );
}

export default EventList;
