import '../Event/Event.scss';
import {Calendar} from "../../components";


function Event() {
    return (
        <>
            <div className="components_wrap">
                <span className='calendar_title'>행사 캘린더</span>
                <Calendar/>
            </div>
        </>
    );
}

export default Event;