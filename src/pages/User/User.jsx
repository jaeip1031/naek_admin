import '../User/User.scss';
import {DataTable} from "../../components";


function User() {
    return (
        <>
            <div className="components_wrap">
                <DataTable/>
            </div>
        </>
    );
}

export default User;