// src/index.js
import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import './fonts/font.css';
import './css/reset.css';
import './css/index.scss';
import './common';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <App />
);

