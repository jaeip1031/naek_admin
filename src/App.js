import React, { useEffect } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { User, Event, EventList, Login, Board } from "./pages";
import Header from './Header';
import Block from './Block';
import RequireAuth from './RequireAuth';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from 'axios';
const API_BASE_URL = process.env.REACT_APP_API_BASE_URL;


function AppContent() {
    useEffect(() => {
        const handleBeforeUnload = async (event) => {
            await axios.post(`${API_BASE_URL}/admin_login/out/logout`, {}, {
                withCredentials: true
            });
        };

        window.addEventListener('beforeunload', handleBeforeUnload);
        return () => {
            window.removeEventListener('beforeunload', handleBeforeUnload);
        };
    }, []);

    return (
        <div className="App">
            <Routes>
                <Route path="/login" element={<Login />} />
                <Route path="*" element={
                    <RequireAuth>
                        <Routes>
                            <Route path="/" element={<><Header /><User /></>} />
                            <Route path="/user" element={<><Header /><User /></>} />
                            <Route path="/eventCalendar" element={<><Header /><Event /></>} />
                            <Route path="/eventList" element={<><Header /><EventList /></>} />
                            <Route path="/board" element={<><Header /><Board /></>} />
                            <Route path="/*" element={<Block />} />
                        </Routes>
                    </RequireAuth>
                } />
            </Routes>
        </div>
    );
}

function App() {
    return (
        <Router>
            <AppContent />
            <ToastContainer
                position="bottom-right" // 알람 위치 지정
                autoClose={3000} // 자동 off 시간
                hideProgressBar={false} // 진행시간바 숨김
                closeOnClick // 클릭으로 알람 닫기
                rtl={false} // 알림 좌우 반전
                pauseOnFocusLoss // 화면을 벗어나면 알람 정지
                draggable // 드래그 가능
                pauseOnHover // 마우스를 올리면 알람 정지
                theme="light"
            />
        </Router>
    );
}

export default App;